import { TemplateValue } from "../src/component.ts";
import { PermissionString } from "./access.ts";


export type RequestType = (
	| 'GET'
	| 'POST'
	| 'POST'
	| 'DELETE'
)

export type RequestObject <P = Record<string, any>> = {
	url: URL;
	params?: P;
	method: RequestType;
	headers: Record<string, string>;
}

export type ResponseObject = {
	body: TemplateValue;
	status: number;
	headers?: Record<string, string>;
}

export type ResponseHandler <T, P = Record<string, any>> = (access: PermissionString | null, request: RequestObject<P>) => T | Promise<T>;

export type ApiRequestHandler <P = Record<string, any>> = ResponseHandler<ResponseObject, P>;
export type PageRequestHandler <P = Record<string, any>> = ResponseHandler<TemplateValue, P>;