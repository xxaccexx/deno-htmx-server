import {
	PageRequestHandler,
	RequestType,
ApiRequestHandler
} from "./requests.ts"

export type APIModule = { [Interface in RequestType]: ApiRequestHandler }
export type PageModule = { default: PageRequestHandler }