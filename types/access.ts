export const accessTypes = ['none', 'get', 'update', 'delete'] as const;
export type Access = typeof accessTypes[number];

export type Permission = {
	api: string;
	feature: string;
	access: string;
}

export type PermissionString = `${string}:${string}:${Access}`;


const validateRecord = <T extends string = any>(name: T) => {
	if (!/[a-z][a-z0-9]+/.test(name)) throw new Error('invalid name');
}

export const stringifyPermission = (permission: Permission): string => {
	const { api, feature, access } = permission;

	validateRecord(api);
	validateRecord(feature);
	validateRecord(access);

	return `${api}:${feature}:${access}`;
}

export const parsePermission = (permission: string): Permission => {
	const [api, feature, access] = permission.split(':').filter(x => x);

	validateRecord(api);
	validateRecord(feature);
	validateRecord(access);

	return {
		api,
		feature,
		access,
	}
}