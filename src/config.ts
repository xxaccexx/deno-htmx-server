import { join } from '../deps.ts'

const importPath = join(Deno.cwd(), 'config.json');
const { default: configFile } = await import('file:/' + importPath, { assert: { type: 'json' } });

export type Config = {
	publicAPIRoute: string;
	routes: {
		static: string;
		api: string;
		pages: string;
	}
}

export const config: Config = {
	publicAPIRoute: configFile?.publicAPIRoute ?? 'api',
	routes: {
		api: configFile?.routes?.api ?? 'routes/api',
		pages: configFile?.routes?.pages ?? 'routes/pages',
		static: configFile?.routes?.static ?? 'routes/static',
	}
}