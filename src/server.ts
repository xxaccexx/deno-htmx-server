import { interpretPath } from './resolve.ts';
import { verifyPaths } from './verifyPaths.ts';
import { getMimeType } from './mime.ts';

import { RequestType } from '../types/requests.ts';
import {
	APIModule,
	PageModule
} from '../types/modules.ts';
import { templateToStream } from "./template.ts";

export const server = (port = 8080, debug = true) => {
	Deno.serve({ port }, async request => {
		const requestUrl = new URL(request.url)
		const { pathname: requestPath } = requestUrl;

		const requestType = request.method as RequestType;
		const requestHeaders: Record<string, string> = Object.fromEntries(request.headers.entries());

		try {
			const interpreted = interpretPath(requestPath);
			const pathname = await verifyPaths(interpreted);

			if (debug) console.log(request.method, requestPath, '=>', JSON.stringify(pathname));
			if (!pathname) throw '404';

			switch (interpreted.type) {
				case 'api': {
					const module = await import(pathname) as APIModule;
					if (!module) throw '404';

					const handler = module[requestType];
					if (!handler) throw '405'

					const resp = await handler(null, {
						url: requestUrl,
						method: requestType,
						headers: requestHeaders,
					});

					if (!resp.body) throw '204';

					const body = templateToStream(resp.body);
					return new Response(body, {
						status: resp.status,
						headers: {
							'max-age': 'no-cache',
							...resp.headers,
						}
					})
				}

				case 'page': {
					const { default: handler } = await import(pathname) as PageModule;
					if (!handler) throw '404'

					const template = await handler(null, {
						url: requestUrl,
						method: requestType,
						headers: requestHeaders
					});

					if (template === null) throw '404';
					else {
						const body = templateToStream(template);

						return new Response(body, {
							status: 200,
							headers: {
								'max-age': '30',
								'content-type': 'text/html'
							}
						})
					}
				}

				case 'static': {
					const file = await Deno.open(pathname);
					return new Response(file.readable, {
						headers: {
							'max-age': '1800',
							'content-type': getMimeType(requestPath),
						}
					});
				}

				default: throw '404';
			}
		}

		catch (e) {
			if (debug) console.error(e);

			switch (e) {
				case '204': {
					return new Response(null, {
						status: 204,
						statusText: 'OK: No content'
					});
				}

				case '404': {
					return new Response('404: Not found', {
						status: 404,
						statusText: 'Not found'
					})
				}

				case '405': {
					return new Response('405: Method not allowed', {
						status: 405,
						statusText: 'Method not allowed'
					});
				}

				default: {
					return new Response('500: Server error', {
						status: 500,
						statusText: 'Server error'
					})
				}
			}
		}
	})
}