import { basename, dirname } from '../deps.ts';
import { extname, join } from '../deps.ts';
import { config } from './config.ts';

export type InterpretedPaths = {
	type: 'api' | 'static' | 'page';
	paths: string[];
}

export const interpretPath = (requestPath: string): InterpretedPaths => {
	if (requestPath === '/') {
		return {
			type: 'page',
			paths: [
				join(Deno.cwd(), config.routes.pages, 'index.ts'),
			]
		}
	}

	if (requestPath.at(0) !== '/') {
		throw new Error('Can only resolve absolute paths');
	}

	const cleanedRequestPath = requestPath.replace('/','');
	const ext = extname(cleanedRequestPath);
	const base = basename(cleanedRequestPath, ext);
	const dir = dirname(cleanedRequestPath);

	let type: InterpretedPaths['type'] = 'page';
	if (dir.startsWith(config.publicAPIRoute)) type = 'api';
	else if (ext) type = 'static'

	switch (type) {
		case 'api': {
			const _dir = dir.replace(config.publicAPIRoute, '');

			return {
				type: 'api',
				paths: [
					join(Deno.cwd(), config.routes.api, _dir, base + '.ts'),
					join(Deno.cwd(), config.routes.api, _dir, base, 'index.ts'),
				]
			}
		}

		case 'static': {
			return {
				type: 'static',
				paths: [
					join(Deno.cwd(), config.routes.static, dir, base + ext),
				]
			}
		}

		case 'page': {
			return {
				type: 'page',
				paths: [
					join(Deno.cwd(), config.routes.pages, dir, base + '.ts'),
					join(Deno.cwd(), config.routes.pages, dir, base, 'index.ts'),
				]
			}
		}
	}
}

export const resolve = (...args: any[]) => ({})