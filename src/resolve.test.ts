import { assertEquals, assertThrows } from '../deps.ts'
import { resolve } from './resolve.ts';

Deno.test({
	name: 'relative path',
	async fn() {
		assertThrows(() => resolve('style/main'))
	}
})

Deno.test({
	name: 'paths without ext',
	async fn() {
		const resolved = resolve('/style/main');
		assertEquals(resolved, {
			isAPI: false,
			paths: [
				'./public/style/main',
				'./public/style/main.html',
				'./public/style/main/index.html'
			]
		})
	}
})

Deno.test({
	name: 'just a root slash',
	async fn() {
		const resolved = resolve('/');
		assertEquals(resolved, {
			isAPI: false,
			paths: [
				'./public',
				'./public.html',
				'./public/index.html'
			]
		})
	}
})

Deno.test({
	name: 'paths with ext',
	async fn() {
		const resolved = resolve('/style/main.css');
		assertEquals(resolved, {
			isAPI: false,
			paths: [
				'./public/style/main.css',
			]
		})
	}
})

Deno.test({
	name: 'api without ext',
	async fn() {
		const resolved = resolve('/api/test');
		assertEquals(resolved, {
			isAPI: true,
			paths: [
				'./api/test.ts',
			]
		})
	}
})

Deno.test({
	name: 'api with ext',
	async fn() {
		assertThrows(() => resolve('/api/test.ts'))
	}
})