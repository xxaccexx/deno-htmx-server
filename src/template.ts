import { TemplateValue } from "./component.ts";

export const resolveTemplate = async (template: TemplateValue, writer: WritableStreamDefaultWriter<string>) => {
	if (!template) writer.write('');
	else if (template instanceof Promise) await resolveTemplate(template, writer);
	else if (typeof template === 'string') writer.write(template);
	else for await (const result of template) {
		await resolveTemplate(result, writer);
	}
}

export const templateToStream = (tmpl: TemplateValue) => {
	const encoder = new TextEncoder();

	const { writable, readable } = new TransformStream<string, Uint8Array>({
		transform: (chunk, ctrl) => ctrl.enqueue(encoder.encode(chunk))
	});

	const writer = writable.getWriter();
	resolveTemplate(tmpl, writer).then(() => writer.close());

	return readable;
}