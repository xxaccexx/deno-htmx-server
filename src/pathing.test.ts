// import { getTree, isValidParam, isValidPath, pathParams } from "@server/src/pathing.ts"
import { assertEquals, assertThrows } from '../deps.ts';

import { getPathSegments } from "@server/src/pathing.ts"

// Deno.test({ name: 'isValidPath - must not wrap with [...]', fn() {
// 	assertEquals(isValidPath('as-d1_23'), true)
// 	assertEquals(isValidPath('[as-d1_23]'), false)
// }})

// Deno.test({ name: 'isValidPath - can start with numebrs', fn() {
// 	assertEquals(isValidPath('as-d1_23'), true)
// }})

// Deno.test({ name: 'isValidPath - cannot contain whitespace', fn() {
// 	assertEquals(isValidPath('a s-d1_23'), false)
// }})

// Deno.test({ name: 'isValidPath - cannot contain specials', fn() {
// 	assertEquals(isValidPath('a$s-d1_23'), false)
// }})


// Deno.test({ name: 'isValidParam - must wrap with [...]', fn() {
// 	assertEquals(isValidParam('as-d1_23'), false)
// 	assertEquals(isValidParam('[as-d1_23]'), true)
// }})

// Deno.test({ name: 'isValidParam - cannot start with numebrs', fn() {
// 	assertEquals(isValidParam('[as d1_23]'), false)
// }})

// Deno.test({ name: 'isValidParam - cannot contain whitespace', fn() {
// 	assertEquals(isValidParam('[as d1_23]'), false)
// }})

// Deno.test({ name: 'isValidParam - cannot contain specials', fn() {
// 	assertEquals(isValidParam('[as$d1_23]'), false)
// }})


// Deno.test({ name: 'toParams - one-to-one pathing', async fn() {
// 	const { matched, params } = pathParams('/asd/asd', new URL('/asd/asd', 'http://localhost:8080'))

// 	assertEquals(matched, true)
// 	assertEquals(params, {});
// }})

// Deno.test({ name: 'toParams - one-to-one pathing', async fn() {
// 	const { matched, params } = pathParams('/asd/[param]', new URL('/asd/value', 'http://localhost:8080'))

// 	assertEquals(matched, true)
// 	assertEquals(params, { param: 'value' });
// }})


// Deno.test({ name: 'getTree', async fn() {
// 	const root = Deno.cwd();
// 	const tree = await getTree('routes', root);

// 	console.log('test', Deno.inspect(tree, { colors: true, depth: Infinity }))
// }})


Deno.test({ name: 'urlPattern', async fn() {
	assertThrows(() => getPathSegments('segment'), 'start with');
	assertThrows(() => getPathSegments('/seg{ment}'), 'not a variable');
	assertThrows(() => getPathSegments('/seg{ment'), 'not a variable');
	assertThrows(() => getPathSegments('/{segment}asd'), 'def has ended');

	assertEquals(
		getPathSegments('/segment/{segment}'),
		[
			{ value: 'segment', variable: false },
			{ value: 'segment', variable: true },
		]
	)
}})

Deno.test({ name: 'validate segments', async fn() {

}})