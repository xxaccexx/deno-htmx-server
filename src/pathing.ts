import { join } from "../deps.ts";

export type PathParams = Record<string, string>;
export type PathParamsMatched = {
	matched: boolean;
	params: PathParams;
}

export const isValidPath = (segment: string): boolean => (
	/^([a-z0-9-_]+)$/i.test(segment)
)

export const isValidParam = (segment: string) => (
	/^\[([a-z]+)([a-z0-9-_]+)?\]$/i.test(segment)
)

export const isValidSegment = (segment: string) => {
	return isValidParam(segment) || isValidPath(segment);
}


export const toParams = (segment: string) => {
	if (!isValidParam(segment)) throw new TypeError('Segment is not a parameter');
	return segment.substring(1, segment.length - 1)
}

export const pathParams = (pattern: string, url: URL): PathParamsMatched => {
	const patternSegs = pattern.split('/').slice(1);

	if (patternSegs.some(seg => !isValidSegment(seg))) {
		throw new TypeError('Pattern is invalid.');
	}

	const { pathname } = url;
	const pathSegs = pathname.split('/');


	if (pathSegs.some(seg => !isValidSegment(seg))) {
		throw new Error('URL does not match pattern')
	}

	const paramNames = (
		patternSegs
			.map((segment, index) => {
				if (isValidParam(segment)) return segment.substring(1, segment.length - 1);
			})
	)

	console.log('segs', patternSegs)
	console.log('parms', paramNames)

	return {
		matched: false,
		params: {}
	}
}


type PathRef = {
	pathname: string;
	children: PathRef[];
}

export const getTree = async (pathname: string, root: string): Promise<PathRef> => {
	const self: PathRef = {
		pathname: pathname,
		children: [],
	}

	for await (const child of await Deno.readDir(pathname)) {
		const childpath = join(pathname, child.name)
		console.log(childpath)

		if (child.isSymlink) self.children.push({
			pathname: childpath,
			children: [],
		})

		if (child.isFile) self.children.push({
			pathname: childpath,
			children: [],
		})

		if (child.isDirectory) self.children.push(
			await getTree(childpath, root)
		)
	}

	return self;
}

const ERR = {
	NO_ROOT: new Error('Invalid URL. Paths must start with "/"'),
	NOT_VAR: new Error('Invalid URL. Segment is not a variable'),
	VAR_END: new Error('Invalid URL. Variable def has ended')
} as const

type PathSegment = {
	variable: boolean;
	value: string;
}
export const getPathSegments = (pattern: string) => {
	const segments: PathSegment[] = [];

	if (pattern.at(0) !== '/') {
		throw ERR.NO_ROOT;
	}

	let ended = false;
	let segment: PathSegment = {
		variable: false,
		value: '',
	}

	for (const char of pattern.substring(1)) {
		if (ended) {
			throw ERR.VAR_END

		} else if (char === '{') {
			if (segment.value) {
				throw ERR.NOT_VAR;
			}
			segment.variable = true;

		} else if (char === '}') {
			if (!segment.variable) {
				throw ERR.NOT_VAR;
			} else {
				ended = true;
			}

		} else if (char === '/') {
			segments.push(segment);
			segment = {
				variable: false,
				value: '',
			}
			ended = false;

		} else {
			segment.value += char;
		}
	}

	if (segment.value.length) segments.push(segment)

	return segments;
}

const segmentMatch = (seg: string, pattern: PathSegment) => {
	if (pattern.variable) {
		return seg === pattern.value;
	} else {
		return true;
	}
}

export const validatePathSegments = (path: string, segments: PathSegment[]) => {
	const segs = path.split('/').filter(x => x);

	if (segs.length !== segments.length) return false;

}