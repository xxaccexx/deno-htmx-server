import { interpretPath } from "./resolve.ts";
import { verifyPaths } from "./verifyPaths.ts";

import { RequestObject } from "../types/requests.ts";
import { APIModule } from "../types/modules.ts";

import { MaybeArray, MaybePromise } from "../types/utils.ts";



type SingleValue = TemplateIterator | string | number | boolean | undefined | null;
type MultiValue = MaybeArray<SingleValue>;

export type TemplateValue = MaybePromise<MaybeArray<MaybePromise<SingleValue> | MaybeArray<MultiValue>>>;
export type TemplateIterator = AsyncGenerator<TemplateValue, void, void>;


export type FC <T extends Record<string, any> | void = void> = (props: T) => TemplateValue;

export async function* html(segments: TemplateStringsArray, ...values: TemplateValue[]): TemplateIterator {
	for (let i = 0; i < segments.length; i++) {
		if (segments[i]) yield segments[i];
		if (values[i]) yield values[i];
	}
}

export async function* apiTmpl(route: string, request: RequestObject): TemplateIterator {
	const interpret = await interpretPath(`/api/${route}`);
	const pathname = await verifyPaths(interpret);

	if (!pathname) yield api404();

	try {
		if (!pathname) throw '500';
		const module = await import(pathname) as APIModule;
		const handler = await module[request.method];

		const result = await handler(null, {
			...request,
			url: new URL(pathname, request.url)
		});

		if (result.status !== 200) yield apiError(result.status,	`API response code`);

		if (result.body === null) yield api404();
		if (Array.isArray(result.body)) {
			for await (const tmpl of result.body) {
				yield tmpl;
			}
		} else {
			yield result.body
		}
	}

	catch (e) {
		console.log('API catch case', e);
		yield apiError(500, e.message ?? e);
	}
}

const apiError = (status: number, statusText: string) => `${status}: ${statusText}`;
const api404 = () => apiError(404, 'Not found');