import { InterpretedPaths } from "./resolve.ts";
import { join } from "../deps.ts";

export const verifyPaths = async (intepreted: InterpretedPaths) => {
	for (const pathname of intepreted.paths) {
		try {
			const file = await Deno.open(pathname);
			const stat = await file.stat();

			if (!stat.isFile) throw new Error('Not found');

			if (intepreted.type === 'static') return pathname;
			return join('file:', pathname);
		}
		catch { continue }
	}

	return null;
}