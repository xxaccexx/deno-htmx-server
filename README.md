# Deno HTMX Server

This module is a simple, http server for use with Deno.

There are 3 main concepts as of now.

# Install

In your importMaps, define `@server/` as `https://gitlab.com/xxaccexx/deno-htmx-server/-/raw/v1.0.0/`

In your entry file

```ts
import { server } from '@server/src/server.ts`
let debugMode: boolean;
server(8080, debugMode);
```

## 1. Server config

There are 3 types of files to serve. which are served from 3 folders.

By default these types + default folders are

1. static files -> `/routes/static`<br/>
	 Requests with extentions like, `.css` or `.html` at the end will route requests to static files.<br/>
	 Find all the extentions handled in this module `@server/src/mime.ts`

2. API files -> `/routes/api`<br/>
	 Requests that start with the public API route `/api` will be routed to API files.<br/>
	 The public route change be controlled from the `config.json` file.

3. Page files -> `/routes/pages`<br/>
	 Files that aren't handled as static or API are assumed to be page requests and routed as such.<br/>
	 Additionally, `/` by itself is routed to `/index.ts`.<br/>
	 Currently, the default file is not configurable.

Additionally to the internal routes, above, the API route also includes a public route interface.<br/>
By default this is set to `/api/...` where the requests after are mapped to the internal routes config.

You can override these values using a `config.json` at the root of your project. This is optional.<br/>
Here is what the default config looks like:
```json
{
	"publicAPIRoute": "api",
	"internal": {
		"static": "routes/static",
		"api": "routes/api",
		"pages": "routes/pages"
	}
}
```

## 2. Templating

Templates are used in APIs, pages and components.

Templating in this framework is done with template string literals tagged with the `html` function.<br/>
The goal here is to reduce values in the template to a string.<br/>
However, it can also handle other templates and `false`, `undefined` and `null` are interpreted as an empty string.

the `html` template tag function resolves the values within to an async iterator, meaning you can use a promise which resolves to the value, and this will be handled by the framework for you.<br/>
this means, things like accessing a database does not effect the content before it, allowing partial documents to be sent to the browser allowing for scipts and styles to load on the browser while the server is resolving the content.

Here is an example of a page:
```ts
import { PageRequestHandler } from "@server/types/requests.ts"
import { html } from '@server/src/component.ts';

const greetings = ['Hello', 'Goodbye'];
const getGreeting = () => new Promise(resolve => {
	setTimeout(
		() => {
			const index = Math.floor(Math.random() * greetings.length);
			resolve(greetings[index]);
		},
		1500
	);
})

const template: PageRequestHandler = async () => html`<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Basic HTMX Example</title>
		<script src="https://unpkg.com/htmx.org@1.9.10" integrity="sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC" crossorigin="anonymous"></script>
	</head>
	<body>
		<h1>${getGreeting()}, World!</h1>
	</body>
</html>
`

export default template
```

You can do anything on the frontend, but since the API routes are also expected to return html, then it is recommended to use [HTMX](https://htmx.org/) which adds functionality via attributes.

Note, in the example above, the `getGreeting` function returns a promise, but does not use the await keyword.<br/>
This allows the rest of the template to resolve, while the promise is handled seperately. The stream however will wait for this value before continuing, keeping the template render order as defined.

Using the await keyword prevents the template from returning anything until all content is resolved.<br/>
This is an implementation detail, which you can choose depending on your usecase.


### Include an API in the page.

You might want to render an API as though it was part of the file already. You can do this using the `apiTmpl` function. Just like the `html` tag function, it is an async generator. Here's an example.

The example API is in the next section.

```ts
import { PageRequestHandler } from "@server/types/requests.ts"
import { html } from '@server/src/component.ts';

const template: PageRequestHandler = async (request) => html`<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Basic HTMX Example</title>
		<script src="https://unpkg.com/htmx.org@1.9.10" integrity="sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC" crossorigin="anonymous"></script>
	</head>
	<body>
		<h1>${apiTmpl('/api/', request)}, World!</h1>
	</body>
</html>
`

export default template
```

The request object is passed through the `PageRequestHandler` interface and provided to the apiTmpl directly.

## 3. API file

An API module must expose a function using one of these HTTP methods: `GET`, `POST`, `UPDATE` or `DELETE`.<br/>
The server will automatically handle 305 error.

Here's an example API file

```ts
import { ApiRequestHandler } from "@server/types/requests.ts";
import { html } from "@server/src/component.ts";

export const GET: ApiRequestHandler = async () => {
	return {
		status: 200,
		body: html`<h1>Hello, World!</h1>`,
		headers: { 'content-type': 'text/html' }
	}
}
```


## 4. Component files

Similar to pages, components use the tagged template string literal syntax. The major difference here is defining properties for the component using the `FC` type interface.

These also resolve to an async generator, so async or not, you can use them in templates as a value.

Here's the same example as above with a promise component.

```ts
import { PageRequestHandler } from "@server/types/requests.ts"
import { FC, html } from '@server/src/component.ts';

const randBool = () => new Promise<boolean>(resolve => setTimeout(() => resolve(Math.random() < .5), 2500));

const getGreeting: FC = async () => html`${await randBool() ? 'Hello' : 'Goodbye'}`

const template: PageRequestHandler = async () => html`<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Basic HTMX Example</title>
		<style>
			* {
				font-family: arial;
				background: lightblue;
			}
		</style>
		<script src="https://unpkg.com/htmx.org@1.9.10" integrity="sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC" crossorigin="anonymous"></script>
	</head>
	<body>
		<h1>${getGreeting()}, World!</h1>
	</body>
</html>
`

export default template
```
